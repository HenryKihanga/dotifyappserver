<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //crete role object and save
        $role = new Role();
        $role->name = 'Admin';
        $role->description='A system administrator';
        $role->save();

        $role = new Role();
        $role->name = 'Artist';
        $role->description='Owner of the Song.The one who has the right to upload the Song.';
        $role->save();



        $role = new Role();
        $role->name = 'Listener';
        $role->description='The one who uses the application for listening music';
        $role->save();
    }
}
