<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);//roles shoul be seeded before user
        $this->call(UsersTableSeeder::class);
        $this->call(AlbumsTableSeeder::class);
        //calling the seeder
        $this->call(GenresTableSeeder::class);
        $this->call(SongsTableSeeder::class);
        //
    }
}
