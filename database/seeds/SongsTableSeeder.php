<?php

use App\Album;
use App\Song;
use Illuminate\Database\Seeder;

class SongsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $album = new Album();
        $album = Album::find(1);
        $song = new Song();
        $song->title = 'Bird set Free';
        $song->cover_file = 'defaults/images/defaultCover.png';
        $song->song_file =  'defaults/songs/Bird Set Free.mp3';
        $song->genre_id = 1;
        $song->viewers = 0;

        $album->songs()->save($song);

        //factory(Model,Number)->create();


        //funcion to call the factory for creating multible data of sonds
        //factory(Song::class ,100)->create();
    }
}
