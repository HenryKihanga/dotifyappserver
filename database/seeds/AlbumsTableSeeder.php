<?php

use App\Album;
use App\User;
use Illuminate\Database\Seeder;

class AlbumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
$user = new User();
        $user = User::find(1);

        $album = new Album();
        $album->name = 'Default';
        $album->cover_file = 'defaults/images/defaultCover.jpeg';
        $album->path_to_file = 'defaults/albums/Default';
        $album->is_default= 1;

        $user->albums()->save($album);


        //calling album factory

        //factory(Album::class, 50)->create();
    }
}
