<?php

use Illuminate\Database\Seeder;
use App\Genre;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //great genre object and save
        $genre = new Genre();
        $genre->name = 'Pop';
        $genre->icon = 'pop_mic.png';
        $genre->save();

        $genre = new Genre();
        $genre->name = 'Rock';
        $genre->icon = 'rock_fire.png';
        $genre->save();

        $genre = new Genre();
        $genre->name = 'Rege';
        $genre->icon = 'pop_mic.png';
        $genre->save();

        $genre = new Genre();
        $genre->name = 'Bongo';
        $genre->icon = 'pop_mic.png';
        $genre->save();

        $genre = new Genre();
        $genre->name = 'HipHup';
        $genre->icon = 'pop_mic.png';
        $genre->save();
    }
}
