<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   $role = Role::where('name' , 'Admin')->first();//find the role in Roles Table

        $user = new User();
        $user->name = 'admin';
        $user->avatar_file = 'Users/admin/Avatar/defaultCover.jpeg';
        $user->email='admin@gmail.com';
        $user->password= bcrypt('admin');//for hashing the password
        $user->save();

        $user ->roles()->attach($role);//attach the fetched role to the saved user
    }
}
