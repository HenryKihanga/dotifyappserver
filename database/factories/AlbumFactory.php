<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Album;
use Faker\Generator as Faker;

$factory->define(Album::class, function (Faker $faker) {
    return [
        'name' =>$faker->text(10),
        'artist_id'=>$faker->randomNumber(2),
        'cover'=>$faker->text(5),
    ];
});
