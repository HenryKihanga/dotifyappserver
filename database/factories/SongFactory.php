<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Song;
use Faker\Generator as Faker;

$factory->define(Song::class, function (Faker $faker) {
    return [
        'title' => $faker->text(10),
        'viewers' => $faker->randomNumber(5),
        'cover_file' => $faker->text(20),
        'song_file' => $faker->text(20),
        'album_id' => $faker->randomNumber(3),
        'genre_id' => $faker->randomNumber(3),
        'is_top_hit' => $faker->boolean(),
        'is_viral' => $faker->boolean(),
        'is_chill_acoustic' => $faker->boolean()


    ];
});
