<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use phpDocumentor\Reflection\Types\Nullable;

class CreateSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('album_id');
            $table->bigInteger('genre_id');
            $table->string('title');
            $table->bigInteger('viewers');
            $table->string('cover_file');
            $table->string('song_file');
            $table->boolean('is_top_hit')->nullable();
            $table->boolean('is_viral')->nullable();
            $table->boolean('is_chill_acoustic')->nullable();
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('songs');
    }
}
