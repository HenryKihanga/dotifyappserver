<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name','user_id','cover','path_to_file','is_default'
    ];

    protected $dates = [
        'deleted_at'

    ];
//relation ship btn album and songs
    public function songs(){
        return $this->hasMany(Song::class);
    }

 //relation ship btn album and user
    public function user(){
        return $this->belongsTo(User::class);
    }
}
