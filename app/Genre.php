<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Genre extends Model
{
    use SoftDeletes;

    protected $fillables = [
        'name', 'icon'
    ];

    protected $dates = [
        'deleted_at'
    ];

//relationship  btn genre and song
    public function songs(){
        return $this->hasMany(Song::class);
    }
}
