<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','avatar_file'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    public function setPasswordAttribute($password)
    {
        if (!empty($password)) {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    //this user belongs to many roles
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
    //this user has many playlists
    public function playlists()
    {
        return $this->hasMany(Playlist::class);
    }

    //this user has many albums
    public function albums()
    {
        return $this->hasMany(Album::class);
    }
    //this user has many songs through album
    public function songs(){
        return $this->hasManyThrough(Song::class , Album::class);
    }
    //function to fetch role of the user
    public function hasRole($role)
    {
        
        if($this->roles()->where('name', $role)->first()) return true;
        return false;
    }
}
