<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Playlist extends Model
{
    use SoftDeletes;

    protected $fillables = [
        'name','user_id','cover_file','path_to_file'
    ];

    protected $dated = [
        'deleted_at'
    ];
    //playlist has many songs
    public function songs()
    {
        return $this->belongsToMany(Song::class);
    }
    //playlist belongs to one user
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
