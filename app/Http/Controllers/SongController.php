<?php

namespace App\Http\Controllers;

use App\Album;
use App\Playlist;
use App\Song;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SongController extends Controller
{
    //method to get all songs
    public function getAllSongs()
    {
        $songs = Song::all();
        return response()->json(['songs' => $songs], 200);
    }

    //method to get a single song
    public function getSong($songId)
    {
        $song = Song::find($songId);
        if (!$song) {
            return response()->json(['error' => 'song do not exist'], 504);
        }

        return response()->json(['song' => $song], 200);
    }
    //method to post a song
    public function postSong(Request $request, $albumId)
    {
        $album = Album::find($albumId);
        if (!$album) {
            return response()->json(['error' => 'no such album found'], 504);
        }

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'genre_id' => 'required',
            'cover_file' => 'required',
            'song_file' => 'required'

        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),

            ], 404);
        }

        //get path of the choosen album
        $path_to_storage = $album->path_to_file;
        //check if request has cover as file
        if ($request->hasFile('cover_file')) {

            $this->coverPath = $request->file('cover_file')->store($path_to_storage . '/Covers');
        } else {
            return response()->json([
                'message' => 'cover should be the file(photo)'
            ], 404);
        }
        //check if request has song as file
        if ($request->hasFile('song_file')) {

            $this->songPath = $request->file('song_file')->store($path_to_storage . '/Songs');
        } else {
            return response()->json([
                'message' => 'song should be the file(Music Audio)'
            ], 404);
        }

        //saving the song details
        $song = new Song();
        $song->title = $request->input('title');
        $song->cover_file = $this->coverPath;
        $song->song_file = $this->songPath;
        $song->genre_id = $request->input('genre_id');
        $song->album_id = 1;
        $song->viewers = 0;
        // $song->save();

        $album->songs()->save($song);

        return response()->json([
            'song' => $song

        ], 200);
    }

    public function putSong(Request $request, $songId)
    {
        //find song in songs by song Id
        $song = Song::find($songId);
        if (!$song) {
            return response()->json(['error' => 'song do not exist'], 504);
        }

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'genre_id' => 'required',
            'album_id' => 'required',
            'viewers' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors(),], 404);
        }
        $song->put([
            'title' => $request->input('title'),
            'genre_id' => $request->input('genre_id'),
            'album_id' => $request->input('album_id'),
            'viewers' => $request->input('viewers'),
        ]);
    }

    public function deleteSong($songId)
    {
        $song = Song::findOrFail($songId);
        if (!$song) {
            return response()->json(['error' => 'song do not exist'], 504);
        }
        $song->delete();

        return response()->json([
            'message' => 'song deleted succesfull'
        ], 200);
    }

    public function addToPlaylist($songId, $playlistId)
    {
        $playlist = Playlist::find($playlistId);
        $song = Song::find($songId);

        $playlist->songs()->save($song);

        return response()->json([
            'playlist' => $playlist
        ]);
    }

    public function viewSongFile($songId)
    {
        $song = Song::find($songId);
        if (!$song) {
            return response()->json([
                'error' => 'Song not Found'
            ], 404);
        }

        $pathToSongFile = storage_path('/app/' . $song->song_file);
        return response()->download($pathToSongFile);
    }

    public function viewCoverFile($songId)
    {
        $song = Song::find($songId);
        if (!$song) {
            return response()->json([
                'error' => 'Cover not Found'
            ], 404);
        }

        $pathToCoverFile = storage_path('/app/' . $song->cover_file);
        return response()->download($pathToCoverFile);
    }
}
