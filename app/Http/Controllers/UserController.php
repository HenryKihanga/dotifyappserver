<?php

namespace App\Http\Controllers;

use App\Album;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $varidator = Validator::make($request->all(), [
            'email' => 'required|unique:users|email',
            'password' => 'required',
            'name' => 'required'

        ]);

        if ($varidator->fails()) {
            return response()->json([
                'errors' => $varidator->errors(),
                'message' => $varidator->errors()->first(),

            ], 404);
        }

        if ($request->hasFile('avatar_file')) {
            $this->path_to_avatar = $request->file('avatar_file')->store('Users/' . $request->name . '/Avatar');
        } else {
            return response()->json([
                'error' => 'avatar must be a file(photo)'
            ], 504);
        }


        $role = Role::where('name', 'Listener')->first(); //find the role in Roles Table
        $user = User::create([
            'name' => $request->name,
            'email'    => $request->email,
            'password' => $request->password,
            'avatar_file' => $this->path_to_avatar,
        ]);
        $user->roles()->attach($role); //attach the fetched role to the saved user
        $token = auth()->login($user); //give user the token

        $user->roles;
        $user->albums;

        return response()->json([
            'user' => $user,
            'token' => $token
        ],200);
    }

    public function login()
    {
        $credentials = request(['name', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 404);
        }

        return response()->json([
            'user' => $credentials,
            'roles' => auth()->user()->roles,
            'albums' => auth()->user()->albums,
            'token' => $token,
        ], 200);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60
        ]);
    }

    public function changeUserRole(Request $request, $userId)
    {
        $user = User::find($userId);
        if (!$user) {
            return response()->json(['error' => 'user do not exist'], 504);
        }

        $oldRole = Role::where('name', $request->input('old_role'))->first();
        $newRole = Role::where('name', $request->input('new_role'))->first();
        if (!$oldRole) {
            return response()->json([
                'message' => 'invalid old role'
            ]);
        }
        if (!$newRole) {
            return response()->json([
                'message' => 'invalid new role'
            ]);
        }
        if ($user->roles()->detach($oldRole)) {
            $user->roles()->attach($newRole);
            Storage::copy('defaults/images/defaultCover.png', 'albums/' . $user->name . '_' . $user->id . '_albums/' . 'default/Covers/defaultCover.jpeg');
            if ($newRole->name == 'Artist') {
                $album = new Album();
                $album->name = $user->name . ' Default';
                $album->path_to_file = 'albums/' . $user->name . '_' . $user->id . '_albums/' . 'default/';
                $album->cover_file = 'albums/' . $user->name . '_' . $user->id . '_albums/' . 'default/Covers/defaultCover.jpeg';
                $album->is_default = true;
                $user->albums()->save($album);
            }
            $user->roles;
            $user->albums;

            return response()->json([
                'user' => $user
            ], 200);
        }

        return response()->json([
            'error' => 'Old Role does not exist'
        ]);
    }

    public function getAllUsers()
    {
        $users = User::all();
        foreach ($users as $user) {
            $user->roles;
            $user->albums;
            $user->songs;
            //assign songs to each album of the user
            foreach ($user->albums as $album) {
                $album->songs;
            }
        }

        return response()->json([
            'users' => $users
        ], 200);
    }

    public function getAllArtists()
    {
        $users = User::all();
        $collection = collect($users)->reject(function ($user) {
            if (!$user->hasRole('Artist'))
                return $user;
        });
        foreach ($collection as $user) {
            $user->roles;
            $user->albums;
            $user->songs;
            //assign songs to each album of the user
            foreach ($user->albums as $album) {
                $album->songs;
            }
        }

        return response()->json(['artists' => $collection], 200);
    }

    public function viewUserAvatar($userId)
    {
        $user = User::find($userId);
        if (!$user) {
            return response()->json([
                'error' => 'user do not exist'
            ], 404);
        }

        $pathToCoverFile = storage_path('/app/' . $user->avatar_file);
        return response()->download($pathToCoverFile);
    }
}
