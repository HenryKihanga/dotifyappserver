<?php

namespace App\Http\Controllers;

use App\Album;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AlbumController extends Controller
{


    //method to get all albums

    public function getAllAlbums()
    {

        $albums = Album::all();
        foreach ($albums as $album) {
            $album->songs;
        };


        // $collection = collect($albums);
        // $filtered = $collection->where('songs', !null);
        // $filtered->all();

        return response()->json(['albums' => $albums], 200);
    }

    public function getUsefullAlbums()
    {
        $albums = Album::all();
        foreach ($albums as $album) {
            $album->songs;
            $album->tracks = 10;
        };

        $collection1 = collect($albums);
        $nonDefaultAlbums = $collection1->where('is_default', false);
        // $nonEmptyAlbums->all();
        $collection2 = collect($nonDefaultAlbums)->reject(function ($album) {
            if (count($album->songs) == 0)
                return $album;
        });

        // $filteredAlbums->all();


        return response()->json(['albums' => $collection2], 200);
    }

    public function postAlbum(Request $request)
    {
        $user = User::find($request->artist_id);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'artist_id'=>'required'


        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),

            ], 404);
        }

        //path to storage for chosen album
        $path_to_storage = 'albums/' . $user->name . '_' . $user->id . '_albums/' . $request->input('name');
        //check if request has cover as file
        if ($request->hasFile('cover_file')) {

            $this->coverPath = $request->file('cover_file')->store($path_to_storage . '/Covers');
        } else {
            return response()->json([
                'message' => 'cover should be the file(photo)'
            ], 404);
        }

        //saving the album details
        $album = new Album();
        $album->name = $request->input('name');
        $album->path_to_file = $path_to_storage;
        $album->cover_file = $this->coverPath;
        $album->is_default = false;


        $user->albums()->save($album);

        return response()->json([
            'album' => $album

        ], 200);
    }

    public function viewAlbumCover($albumId)
    {
        $album = Album::find($albumId);
        if (!$album) {
            return response()->json([
                'error' => 'Cover not Found'
            ], 404);
        }

        $pathToCoverFile = storage_path('/app/' . $album->cover_file);
        return response()->download($pathToCoverFile);
    }
}
