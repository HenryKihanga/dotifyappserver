<?php

namespace App\Http\Controllers;

use App\Playlist;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PlaylistController extends Controller
{
    public function getAllPlaylists()
    {
        $playlists = Playlist::all();
        foreach ($playlists as $playList) {
            $playList->songs;
        }

        return response()->json([
            'playlists' => $playlists
        ], 200);
    }

    public function getPlaylist($playlistId)
    {
        $playlist = Playlist::find($playlistId);
        $playlist->songs;
        if (!$playlist) {
            return response()->json([
                'error' => 'playlist do not exist'
            ], 404);
        }
        return response()->json([
            'playlist' => $playlist
        ], 200);
    }

    public function createPlaylist(Request $request, $userId)
    {
        $user = User::find($userId);
        if (!$user) {
            return response()->json([
                'error' => 'user do not exist'
            ], 404);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            response()->json([
                'error' => $validator->errors(),
            ], 404);
        }

        //path to storage for chosen album
        $path_to_storage = 'playlists/' . $user->name . '_' . $user->id . '_playlists/' . $request->input('name');
        if ($request->hasFile('cover_file')) {
            $this->coverPath = $request->file('cover_file')->store($path_to_storage . '/Covers');
        } else {
            return response()->json([
                'message' => 'Cover must be a file(photo)'
            ], 404);
        }
        $playList = new Playlist();
        $playList->name = $request->input('name');
        $playList->cover_file = $this->coverPath;
        $playList->path_to_file = $path_to_storage;

        $user->playlists()->save($playList);


        return response()->json([
            'playlist' => $playList

        ], 200);
    }

    public function viewPlaylistCover($playlistId)
    {
        $playList = Playlist::find($playlistId);
        if (!$playList) {
            return response()->json([
                'error' => 'playlist do not exist'
            ], 404);
        }

        $pathToCoverFile = storage_path('/app/' . $playList->cover_file);
        return response()->download($pathToCoverFile);
    }
}
