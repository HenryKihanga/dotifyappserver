<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckRole
{
    public function handle($request, Closure $next, $role)
    {
        $user = User::find($request->artist_id);
        if (!$user) return response()->json([
            'error' => 'user not exist'
        ]);
        if (!$user->hasRole($role)) return response()->json([
            'error' => 'you dont have permission to post album'
        ], 404);

        return $next($request);
    }
}
