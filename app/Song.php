<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Song extends Model
{
    use SoftDeletes;
    protected $fillables = [
        'title', 'viewers','song_file',
        'cover_file', 'album_id',
        'genre_id', 'is_top_hit',
        'is_viral', 'is_chill_acoustic',
    ];

    protected $dates = [
        'deleted_at'

    ];

    //song belongs to an album
    public function album()
    {
        return $this->belongsTo(Album::class);
    }

    //song belongs to genre
    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }

    //song has many playlist

    public function playlists()
    {
        return $this->belongsToMany(Playlist::class);
    }
}
