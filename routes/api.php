<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Routes for UserControler
Route::post('/register', 'UserController@register');
Route::post('/login', 'UserController@login');
Route::post('/logout', 'UserController@logout');
Route::get('users', ['uses'=>'UserController@getAllUsers']);
Route::get('artists', ['uses'=>'UserController@getAllArtists']);
Route::post('changeUserRole/{UserId}' , ['uses'=>'UserController@changeUserRole']);
Route::get('user/viewUserAvatar/{userId}', ['uses'=>'UserController@viewUserAvatar']);

//Routes for SongController
Route::get('songs', ['uses'=>'SongController@getAllSongs']);
Route::post('song/{albumId}', ['uses'=>'SongController@postSong']);
Route::get('song/{songId}', ['uses'=>'SongController@getSong']);
Route::put('song/{songId}' ,['uses'=>'SongController@putSong']);
Route::delete('song/{songId}', ['uses'=>'SongController@deleteSong']);
Route::get('song/viewSong/{SongId}', ['uses'=>'SongController@viewSongFile']);
Route::get('song/viewSongCover/{SongId}', ['uses'=>'SongController@viewCoverFile']);
Route::post('addSongtoPlaylist/{songId}/{playlistId}', ['uses'=>'SongController@addToPlaylist']);


//Routes for AlbumController
Route::get('albums', ['uses'=>'AlbumController@getAllAlbums']);
Route::get('filteredAlbums', ['uses'=>'AlbumController@getUsefullAlbums']);
Route::post('album', ['uses'=>'AlbumController@postAlbum'])->middleware('checkRole:Artist');
Route::get('album/viewAlbumCover/{albumId}', ['uses'=>'AlbumController@viewAlbumCover']);

//Routes for PlayListController
Route::get('playlists',['uses'=>'PlaylistController@getAllPlaylists']);
Route::get('playlist/{playListId}',['uses'=>'PlaylistController@getPlaylist']);
Route::post('createPlaylist/{userId}',['uses'=>'PlaylistController@createPlaylist']);
Route::get('playlist/viewCover/{playlistId}', ['uses'=>'PlaylistController@viewPlaylistCover']);
